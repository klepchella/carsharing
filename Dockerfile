## Multistage image, based on https://pythonspeed.com/articles/multi-stage-docker-python/

FROM python:3.10-slim

WORKDIR /carsharing

COPY ./requirements.txt /carsharing/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /carsharing/requirements.txt

COPY ./app /carsharing/app
COPY ./app /carsharing/carsharing
COPY ./app /carsharing/settings
COPY ./app /carsharing/tests

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
