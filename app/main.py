from fastapi import FastAPI
from carsharing.app.users import models as user_models
from carsharing.app.cars import models as car_models
from .database import SessionLocal, engine
from carsharing.api.users.views import api_router as car_router
from carsharing.api.users.views import api_router as user_router


user_models.Base.metadata.create_all(bind=engine)
car_models.Base.metadata.create_all(bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


app = FastAPI()
app.include_router(car_router, tags=["car"])
app.include_router(user_router, tags=["user"])

# TODO: 1. Подцепить запросы
#  3. Done: Перенести запуск проекта и БД проекта в докер
#  4. Написать доку


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}
