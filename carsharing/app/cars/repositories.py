from dataclasses import dataclass

from sqlalchemy import select
from sqlalchemy.sql import Select

from app.database import Base
from carsharing.app.cars.models import (
    Car,
    Sharing,
    UserCarStatistic,
    Brand,
    CarModel,
    Place,
)
from carsharing.app.users.models import User


@dataclass
class SharingRepository:
    db: Base

    async def select_query(self, query: Select):
        async with self.db.session() as session:
            data = await session.execute(query)
            return data.unique().scalars().all()

    async def get_all_active_cars(self):
        query = select(Car).join(Sharing).where(Sharing.is_active == True)

        return self.select_query(query)

    async def get_statistic_by_user(self, user_id: int):
        query = (
            select(UserCarStatistic)
            .join(User)
            .join(Car)
            .join(CarModel)
            .join(Brand)
            .join(Place)
            .where(UserCarStatistic.user_id == user_id)
        )
        return self.select_query(query)

    async def create_record(self, record: Base):
        async with self.db.session() as session:
            async with session.begin():
                session.add(record)
            await session.refresh(record)
        return record

    async def update_record(self, record: Base):
        async with self.db.session() as session:
            async with session.begin():
                # todo: update
                session.add(record)
            await session.refresh(record)
        return record

    async def delete_record(self, record: Base):
        async with self.db.session() as session:
            async with session.begin():
                # todo: delete
                session.add(record)
            await session.refresh(record)
        return record
