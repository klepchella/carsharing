import datetime

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import (
    ENUM,
    VARCHAR,
    TIMESTAMP,
    INTEGER,
    BOOLEAN,
    DOUBLE_PRECISION,
)
from sqlalchemy.orm import relationship

from carsharing.app.cars.enums import PeriodEnum
from carsharing.app.users.models import User
from app.database import Base


class Place(Base):
    __tablename__ = "place"
    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    name = Column("name", VARCHAR(length=100), nullable=False, doc="Название в системе")
    address = Column("address", VARCHAR(length=100), nullable=False, doc="Адрес")


class Brand(Base):
    """Марка автомобиля"""

    __tablename__ = "brand"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    name = Column("name", VARCHAR(length=100), nullable=False, doc="Название марки")
    description = Column(
        "description", VARCHAR(length=400), nullable=True, doc="Описание марки"
    )


class CarModel(Base):
    """Модель автомобиля"""

    __tablename__ = "car_model"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    brand_id = Column(
        "brand_id", INTEGER, ForeignKey(Brand.id, ondelete="CASCADE"), nullable=False
    )
    name = Column("name", VARCHAR(length=100), nullable=False, doc="Название модели")
    description = Column(
        "description", VARCHAR(length=400), nullable=True, doc="Описание модели"
    )

    brand = relationship(Brand, lazy="raise", backref="car_models")


class Car(Base):
    """Автомобиль"""

    __tablename__ = "car"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    model_id = Column(
        "model_id", INTEGER, ForeignKey(CarModel.id, ondelete="CASCADE"), nullable=False
    )
    number = Column("number", VARCHAR(length=8), doc="Государственный номер автомобиля")
    created_at = Column(
        TIMESTAMP, default=datetime.datetime.now(), doc="Дата создания записи"
    )
    release_date_at = Column(TIMESTAMP, doc="Дата выпуска автомобиля")
    garage_id = Column(
        "garage_id", INTEGER, ForeignKey(Place.id, ondelete="CASCADE"), nullable=False
    )

    model = relationship(CarModel, lazy="raise", backref="cars")
    garage = relationship(Place, lazy="raise", backref="garage")


class Sharing(Base):
    """Состояние автомобиля"""

    __tablename__ = "sharing"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    car_id = Column(
        "car_id", INTEGER, ForeignKey(Car.id, ondelete="CASCADE"), nullable=False
    )
    is_active = Column("is_active", BOOLEAN, doc="Доступен ли автомобиль для аренды")

    car = relationship(Car, lazy="raise", backref="sharing")


class CarPriceForPeriod(Base):
    """Цена аренды автомобиля за период"""

    __tablename__ = "car_price_for_period"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    car_id = Column(
        "car_id", INTEGER, ForeignKey(Car.id, ondelete="CASCADE"), nullable=False
    )
    period = Column(
        "period",
        ENUM(PeriodEnum, name="period_enum"),
        nullable=False,
        doc="Период аренды",
    )
    price = Column("price", DOUBLE_PRECISION, doc="Цена за единицу времени")

    car = relationship(Car, lazy="raise", backref="sharing")


class UserCarStatistic(Base):
    """Статистика Пользователь-Автомобиль"""

    __tablename__ = "user_car_statistic"

    id = Column("id", INTEGER, primary_key=True, autoincrement=True)
    car_id = Column(
        "car_id", INTEGER, ForeignKey(Car.id, ondelete="CASCADE"), nullable=False
    )
    user_id = Column(
        "user_id", INTEGER, ForeignKey(User.id, ondelete="CASCADE"), nullable=False
    )
    price_for_period = Column(
        "price_for_period",
        INTEGER,
        ForeignKey(CarPriceForPeriod.id, ondelete="CASCADE"),
        nullable=False,
        doc="Цена аренды за период",
    )
    created_at = Column(
        "created_at", TIMESTAMP, nullable=False, doc="Дата создания записи"
    )
    started_at = Column(
        "started_at", TIMESTAMP, nullable=False, doc="Дата начала аренды"
    )
    finished_at = Column(
        "finished_at",
        TIMESTAMP,
        nullable=False,
        onupdate=datetime.datetime.now(),
        doc="Дата окончания аренды",
    )
    place_begin_id = Column(
        "place_begin_id",
        INTEGER,
        ForeignKey(Place.id, ondelete="CASCADE"),
        nullable=False,
        doc="Место начала аренды",
    )
    place_end_id = Column(
        "place_end_id",
        INTEGER,
        ForeignKey(Place.id, ondelete="CASCADE"),
        nullable=True,
        doc="Место окончания аренды",
    )

    car = relationship(Car, lazy="raise", backref="user_car_statistics")
    user = relationship(User, lazy="raise", backref="user_car_statistics")
    place_begin = relationship(Place, lazy="raise", backref="user_car_statistics_begin")
    place_end = relationship(Place, lazy="raise", backref="user_car_statistics_end")
