import enum


class PeriodEnum(enum.Enum):

    minute = 1
    day = 2
    week = 3
    month = 4
    year = 5
