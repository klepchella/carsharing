import datetime

from sqlalchemy import Column, Integer
from sqlalchemy.dialects.postgresql import VARCHAR, TIMESTAMP

from app.database import Base


class User(Base):
    """Пользователь сервиса"""

    __tablename__ = "user"

    id = Column("id", Integer, primary_key=True, autoincrement=True)
    first_name = Column("first_name", VARCHAR(length=100), nullable=False)
    middle_name = Column("middle_name", VARCHAR(length=100), nullable=False)
    last_name = Column("last_name", VARCHAR(length=100), nullable=False)
    created_at = Column(
        "created_at", TIMESTAMP, nullable=False, onupdate=datetime.datetime.now()
    )
