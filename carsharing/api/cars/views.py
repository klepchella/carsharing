from fastapi import APIRouter

api_router = APIRouter()

# todo: при создании записей учесть гонку
#  и кейс когда юзер тапает несколько раз по одной кнопке


@api_router.get("/car", name="car:get_all_cars", tags=["car"])
async def get_all_cars():
    return "cars"


@api_router.get("/car/available", name="car:get_available_cars", tags=["car"])
async def get_available_cars():
    return "cars"
