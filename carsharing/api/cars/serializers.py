from typing import Optional

from pydantic import BaseModel, Field


class UserSerializer(BaseModel):
    id: int = Field(description="ID пользователя")
    first_name: str = Field(description="Имя")
    middle_name: Optional[str] = Field(description="Отчество")
    last_name: str = Field(description="Фамилия")
