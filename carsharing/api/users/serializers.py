from datetime import datetime
from decimal import Decimal
from typing import Optional

from pydantic import BaseModel, Field

from carsharing.api.cars.serializers import UserSerializer


class PlaceSerializer(BaseModel):
    id: int = Field(description="ID")
    name: str = Field(description="Название")
    address: int = Field(description="Адрес")


class BrandSerializer(BaseModel):
    id: int = Field(description="ID")
    name: str = Field(description="Название")
    description: Optional[str] = Field(description="Описание бренда")


class CarModelSerializer(BaseModel):
    id: int = Field(description="ID")
    name: str = Field(description="Название")
    brand_id: int = Field(description="ID бренда")
    description: Optional[str] = Field(description="Описание модели")

    brand: BrandSerializer


class CarSerializer(BaseModel):
    id: int = Field(description="ID")
    model_id: int = Field(description=" ID")
    number: str = Field(description="Государственный номер автомобиля")
    release_date_ad: datetime = Field(description="Дата выпуска автомобиля")
    garage_id: int = Field(description="ID гаража (места прикрепления автомобиля)")

    model: CarModelSerializer
    garage: PlaceSerializer


class SharingSerializer(BaseModel):
    id: int = Field(description="ID")
    car_id: int = Field(description="ID автомобиля")
    is_active: bool = Field(description="Доступен ли автомобиль в аренду")

    car: CarSerializer


class CarPriceForPeriodSerializer(BaseModel):
    id: int = Field(description="ID")
    car_id: int = Field(description="ID автомобиля")
    period: int = Field(description="Период аренды")
    price: Decimal = Field(description="Цена аренды автомобиля за период (руб.)")

    car: CarSerializer


class UserCarStatisticSerializer(BaseModel):
    id: int = Field(description="ID")
    car_id: int = Field(description="ID автомобиля")
    user_id: int = Field(description="ID пользователя")
    price_for_period: int = Field(description="Период аренды")
    started_at: datetime = Field(description="Дата начала аренды")
    finished_at: datetime = Field(description="Дата окончания аренды")
    place_begin_id: int = Field(description="ID места начала аренды")
    place_end_id: int = Field(description="ID места окончания аренды")

    user: UserSerializer
    car: CarSerializer
    place_begin: PlaceSerializer
    place_end: PlaceSerializer
