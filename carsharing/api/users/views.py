from fastapi import APIRouter

api_router = APIRouter()


@api_router.get("/user", name="user:get_all_users", tags=["user"])
async def get_all_users():
    return "user"


@api_router.get("/user/statistic", name="user:get_user_statistic", tags=["user"])
async def get_user_statistic():
    return "stats"
